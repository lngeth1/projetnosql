"""projetnosql URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import index, dogs_search, add_dog, dogs_list, remove_dog_psql, remove_dog_mongo

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='/'),
    path('dogs/search', dogs_search, name='dogs_search'),
    path('add_dog', add_dog, name='add_dog'),
    path('remove_dog_psql', remove_dog_psql, name='remove_dog_psql'),
    path('remove_dog_mongo', remove_dog_mongo, name='remove_dog_mongo'),
    path('dogs_list/', dogs_list, name='dogs_list'),
]